﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccess;
using Domain.Entities.StoredProcedures;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestEntityFrameworkRelations.StoredProcedureOperations
{
    [TestClass]
    public class CrudOperationsTests
    {

        [TestMethod]
        public void Insert()
        {
            using (var dbContext = new StoredProceduresDbContext())
            {

                var entity = new StoredProcedureEntity { Age = 10, Name = "TestInserted" };
                dbContext.StoredProcedureEntitySet.Add(entity);
                dbContext.SaveChanges();


            }


            using (var dbContext = new StoredProceduresDbContext())
            {
                StoredProcedureEntity addEntity = dbContext.StoredProcedureEntitySet.First(x => x.Name == "TestInserted");
                dbContext.StoredProcedureEntitySet.Remove(addEntity);
                dbContext.SaveChanges();
            }
        }


        [TestMethod]
        public void Update()
        {
            using (var dbContext = new StoredProceduresDbContext())
            {
                StoredProcedureEntity entity = new StoredProcedureEntity { Id = 1, Age = 10, Name = "TestValue1" };
                dbContext.StoredProcedureEntitySet.Attach(entity);
                dbContext.Entry(entity).State = EntityState.Modified;
                dbContext.SaveChanges();
            }
        }



        [TestMethod]
        public void Delete()
        {
            using (var dbContext = new StoredProceduresDbContext())
            {
                StoredProcedureEntity entity = new StoredProcedureEntity { Age = 11, Name = "TestToDelete" };
                dbContext.StoredProcedureEntitySet.Add(entity);
                dbContext.SaveChanges();
            }

            using (var dbContext = new StoredProceduresDbContext())
            {
                List<StoredProcedureEntity> entityList = dbContext.StoredProcedureEntitySet.Where(x => x.Name == "TestToDelete").ToList();
                dbContext.StoredProcedureEntitySet.Remove(entityList[entityList.Count() - 1]);
                dbContext.SaveChanges();
            }
        }



        [TestMethod]
        public void Read()
        {
            using (var dbContext = new StoredProceduresDbContext())
            {
                List<StoredProcedureEntity> entityList = dbContext.StoredProcedureEntitySet.ToList();
                Assert.IsNotNull(entityList);
            }
        }
    }
}
