﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using Domain.Entities.OneToOne.OneDirectional;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestEntityFrameworkRelations.BasicCreatOperations.OneToOneHasRequired
{
    [TestClass]
    public class OneToOneSingleDirection
    {
        [TestCleanup]
        public void DeleteItemsInMasterWithRequiredSimpleSlave()
        {
            using (var dbContext = new OneToOneRequiredDbContext())
            {
               dbContext.MasterWithRequiredSimpleSlaveSet
                   .SqlQuery(" Delete from [EntityRelationsTestsDB].[dbo].[MasterWithRequiredSimpleSlave] Where Id > 2");
            } 
        }

        [TestMethod]
        public void CreateItem_NullSlave_ItemAdded()
        {
            int previousCount = 0;
            using (var dbContext = new OneToOneRequiredDbContext())
            {
                SimpleSlave slave = dbContext.SimpleSlaveSet.First();
                MasterWithRequiredSimpleSlave newItem = new MasterWithRequiredSimpleSlave()
                {
                    Name = "test1",
                    Slave = slave
                };
                previousCount = dbContext.MasterWithRequiredSimpleSlaveSet.Count();

                dbContext.MasterWithRequiredSimpleSlaveSet.Add(newItem);
                dbContext.SaveChanges();
            }

            using (var dbContext = new OneToOneRequiredDbContext())
            {
                var list = dbContext.MasterWithRequiredSimpleSlaveSet.ToList();
                var count = list.Count;
                Assert.AreEqual(previousCount + 1, count);
            }
        }

        [TestMethod]
        public void GetSimpleSlave()
        {
            using (var dbContext = new OneToOneRequiredDbContext())
            {
                List<SimpleSlave> items = dbContext.SimpleSlaveSet.ToList();
                Assert.IsNotNull(items[0]);
            }
        }
       
    }
}