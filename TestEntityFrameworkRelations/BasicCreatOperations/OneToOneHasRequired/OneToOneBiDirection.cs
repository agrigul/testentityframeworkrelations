﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using Domain.Entities.OneToOne.BiDirectional;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestEntityFrameworkRelations.BasicCreatOperations.OneToOneHasRequired
{
    [TestClass]
    public class OneToOneBiDirection
    {

        [TestMethod]
        public void GetMasterEqualToSlaveById()
        {
            using (var dbContext = new OneToOneRequiredDbContext())
            {
                dbContext.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);

                List<MasterWithRequiredSlave> items = dbContext.MasterWithRequiredSlaveSet.ToList();
                Assert.IsNotNull(items[0].Slave);
                Assert.IsTrue(items[0].Slave.Name != "");
                Assert.IsTrue(items[0].Name != "");
            }
        }

        [TestMethod]
        public void GetMasterWithSlaveIdSet()
        {
            using (var dbContext = new OneToOneRequiredDbContext())
            {
                List<MasterWithRequiredSlave> items = dbContext.MasterWithRequiredSlaveSet.ToList();
                Assert.IsNotNull(items[1].Slave);
                Assert.IsTrue(items[1].Slave.Name != "");
                Assert.IsTrue(items[1].Name != "");
            }
        }


        [TestMethod]
        public void GetSlavesWithRequiredMaster()
        {
            using (var dbContext = new OneToOneRequiredDbContext())
            {
                List<SlaveWithRequiredMaster> items = dbContext.SlaveWithRequiredMasterSet.ToList();
                Assert.IsNotNull(items[0].Master);
            }
        }
        
    }
}
