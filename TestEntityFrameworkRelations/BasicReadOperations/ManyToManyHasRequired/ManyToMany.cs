﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using Domain.Entities.ManyToMany;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestEntityFrameworkRelations.BasicReadOperations.ManyToManyHasRequired
{
    [TestClass]
    public class ManyToMany
    {

        [TestMethod]
        public void GetMastersWithManySlaves()
        {
            using (var dbContext = new ManyToManyRequiredDbContext())
            {
                List<MasterWithRequiredSlaveList> items = dbContext.MastersWithRequiredSlaveListSet.ToList();
                Assert.IsNotNull(items[0].SlavesList);
                Assert.IsNotNull(items[0].SlavesList[1]);

            }
        }


        [TestMethod]
        public void GetSlavesWithManyMaster()
        {
            using (var dbContext = new ManyToManyRequiredDbContext())
            {
                List<SlaveWithRequiredMasterList> items = dbContext.SlavesWithRequiredMasterListSet.ToList();
                Assert.IsNotNull(items[0].MasterList);
                Assert.IsNotNull(items[0].MasterList[0]);
            }
        }

    }
}
