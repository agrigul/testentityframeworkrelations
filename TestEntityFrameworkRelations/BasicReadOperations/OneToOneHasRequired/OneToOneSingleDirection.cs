﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using Domain.Entities.OneToOne.OneDirectional;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestEntityFrameworkRelations.BasicReadOperations.OneToOneHasRequired
{
    [TestClass]
    public class OneToOneSingleDirection
    {

        [TestMethod]
        public void GetMasterWithRequiredSimpleSlave()
        {
            using (var dbContext = new OneToOneRequiredDbContext())
            {
                List<MasterWithRequiredSimpleSlave> items = dbContext.MasterWithRequiredSimpleSlaveSet.ToList();
                Assert.IsTrue(items[0].Slave.Name == "SlaveWithMaster In Single Direction");
            }
        }

        [TestMethod]
        public void GetSimpleSlave()
        {
            using (var dbContext = new OneToOneRequiredDbContext())
            {
                List<SimpleSlave> items = dbContext.SimpleSlaveSet.ToList();
                Assert.IsNotNull(items[0]);
            }
        }
       
    }
}