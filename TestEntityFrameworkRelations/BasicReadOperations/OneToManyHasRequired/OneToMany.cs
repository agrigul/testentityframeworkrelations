﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using Domain.Entities.OneToMany;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestEntityFrameworkRelations.BasicReadOperations
{
    [TestClass]
    public class OneToMany
    {

        [TestMethod]
        public void GetMasterWithSeveralSlaves()
        {
            using (var dbContext = new OneToManyRequiredDbContext())
            {
                List<MasterWithRequiredSlaveList> items = dbContext.MasterWithRequiredSlaveListSet.ToList();
                Assert.IsNotNull(items[0].SlavesList);
                Assert.IsNotNull(items[0].SlavesList[1]);

            }
        }


        [TestMethod]
        public void GetSlavesWithOneMaster()
        {
            using (var dbContext = new OneToManyRequiredDbContext())
            {
                List<SlaveWithRequiredMaster> items = dbContext.SlaveWithRequiredMasterSet.ToList();
                Assert.IsNotNull(items[0].Master);
                Assert.IsNotNull(items[0].Name);
            }
        }

    }
}
