﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using Domain.Entities.OneToZeroOrOne;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestEntityFrameworkRelations.BasicReadOperations.OneToOneHasOptional
{
    [TestClass]
    public class OneToOneBiDirection
    {

        [TestMethod]
        public void GetMasterEqualToSlaveById()
        {
            using (var dbContext = new OneToOneOptionalDbContext())
            {
                List<MasterWithOptionalSlave> items = dbContext.MasterWithOptionalSlaveSet.ToList();
                Assert.IsNotNull(items[0].Slave);
                Assert.IsTrue(items[0].Slave.Name != "");
                Assert.IsTrue(items[0].Name != "");
            }
        }

        [TestMethod]
        public void GetMasterWithSlaveIdSet()
        {
            using (var dbContext = new OneToOneOptionalDbContext())
            {
                List<MasterWithOptionalSlave> items = dbContext.MasterWithOptionalSlaveSet.ToList();
                Assert.IsNotNull(items[1].Slave);
                Assert.IsTrue(items[1].Slave.Name != "");
                Assert.IsTrue(items[1].Name != "");
            }
        }


        [TestMethod]
        public void GetSlavesWithRequiredMaster()
        {
            using (var dbContext = new OneToOneOptionalDbContext())
            {
                List<SlaveWithOptionalMaster> items = dbContext.SlaveWithOptionalMasterSet.ToList();
                Assert.IsNotNull(items[0].Master);
            }
        }
        
    }
}
