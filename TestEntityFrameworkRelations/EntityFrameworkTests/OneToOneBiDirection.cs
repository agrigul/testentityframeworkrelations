﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using Domain.Entities.TestingEntityFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestEntityFrameworkRelations.EntityFrameworkTests
{
    [TestClass]
    public class EfTests
    {

        [TestMethod]
        public void GetStudent()
        {
            using (var dbContext = new TestEntityFrameworkDbContext())
            {
                dbContext.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);

                List<Student> items = dbContext.StudentsSet.ToList();
                Assert.IsNotNull(items[0].Book);
                Assert.IsTrue(items[0].Book.BookName != "");
                Assert.IsTrue(items[0].StudentName != "");
            }
        }
        
    }
}
