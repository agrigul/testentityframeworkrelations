namespace Domain.Entities.OneToOne.OneDirectional
{
    // one-to-one  relations
    public class MasterWithRequiredSimpleSlave : Entity
    {
        public virtual SimpleSlave Slave { get; set; } // can be null or one
        public string Name { get; set; }
    }
}