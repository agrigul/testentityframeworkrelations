namespace Domain.Entities.OneToOne.BiDirectional
{
    public class SlaveWithRequiredMaster : Entity
    {
        public string Name { get; set; }
        public virtual MasterWithRequiredSlave Master { get; set; }
    }
}