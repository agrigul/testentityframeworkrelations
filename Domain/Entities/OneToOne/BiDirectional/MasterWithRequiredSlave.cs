namespace Domain.Entities.OneToOne.BiDirectional
{
    // one-to-one  relations
    public class MasterWithRequiredSlave : Entity
    {
        public virtual SlaveWithRequiredMaster Slave { get; set; } // can be null or one
        public string Name { get; set; }
    }
}