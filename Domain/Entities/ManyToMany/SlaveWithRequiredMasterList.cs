using System.Collections.Generic;

namespace Domain.Entities.ManyToMany
{
    public class SlaveWithRequiredMasterList : Entity
    {
        public string Name { get; set; }
        public virtual IList<MasterWithRequiredSlaveList> MasterList { get; set; }
       // public int MasterId { get; set; }
    }
}