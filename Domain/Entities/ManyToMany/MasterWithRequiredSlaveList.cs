using System.Collections.Generic;


namespace Domain.Entities.ManyToMany
{
    public class MasterWithRequiredSlaveList : Entity
    {
        public string Name { get; set; }
        public virtual IList<SlaveWithRequiredMasterList> SlavesList { get; set; }
        // public int SlaveId { get; set; }
    }
}