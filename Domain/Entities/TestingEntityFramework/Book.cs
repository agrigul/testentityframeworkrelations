namespace Domain.Entities.TestingEntityFramework
{
    public class Book 
    {
        public int BookId { get; set; }
        public string BookName { get; set; }
        public virtual Student Student { get; set; }
    }
}