namespace Domain.Entities.TestingEntityFramework
{
    
    public class Student 
    {
        public int StudentId { get; set; }
        public virtual Book Book { get; set; } // can be null
        public string StudentName { get; set; }
    }
}