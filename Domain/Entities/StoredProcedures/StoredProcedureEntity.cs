﻿namespace Domain.Entities.StoredProcedures
{
    public class StoredProcedureEntity : Entity
    {
        public string Name { get; set; }
        public int Age { get; set; }
        
    }
}
