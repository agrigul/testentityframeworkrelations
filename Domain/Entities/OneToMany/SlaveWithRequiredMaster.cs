using System.Collections.Generic;
using System.Data;

namespace Domain.Entities.OneToMany
{
    public class SlaveWithRequiredMaster : Entity
    {
        public string Name { get; set; }
        
        public virtual MasterWithRequiredSlaveList Master { get; set; }
        // public int MasterId { get; set; }   // comment when use Independent association instead of FK association.
    }
}