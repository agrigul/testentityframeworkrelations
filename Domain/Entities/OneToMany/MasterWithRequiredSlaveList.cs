using System.Collections.Generic;

namespace Domain.Entities.OneToMany
{
    public class MasterWithRequiredSlaveList : Entity
    {
        public virtual IList<SlaveWithRequiredMaster> SlavesList { get; set; }
        public string Name { get; set; }
    }
}