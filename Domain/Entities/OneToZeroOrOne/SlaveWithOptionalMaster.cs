namespace Domain.Entities.OneToZeroOrOne
{
    public class SlaveWithOptionalMaster : Entity
    {
        public string Name { get; set; }
        public virtual MasterWithOptionalSlave Master { get; set; }
    }
}