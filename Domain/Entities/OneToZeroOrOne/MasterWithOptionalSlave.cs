namespace Domain.Entities.OneToZeroOrOne
{
    // one-to-zero or one relations
    public class MasterWithOptionalSlave : Entity
    {
        public virtual SlaveWithOptionalMaster Slave { get; set; } // can be null
        public string Name { get; set; }
    }
}