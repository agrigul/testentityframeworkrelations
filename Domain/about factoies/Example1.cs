﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.about_factoies
{
    public class Grid
    {
        public object rows;
        public object culumns;
        public void SomeLogic(){}
    }

    public class GridA : Grid
    {
        public void SomeLogicForA(){}
    }

    public class GridB : Grid
    {
        public void SomeLogicForB(){}
    }

    public class Tab
    {
        public Grid Grid { get; set; }
        public object Dropdown { get; set; }

        public Tab(Grid grid, object dropdown)
        {
            Grid = grid;
            Dropdown = dropdown;

        }
    }
    public class TabsFactory
    {
        public Tab createTabWithAGrid()
        {
            var grid = new GridA(); // difference
            object dropdown = new object();
            return new Tab(grid, dropdown);
        }


        public Tab createTabWithBGrid()
        {
            var grid = new GridB();  // difference
            object dropdown = new object();
            return new Tab(grid, dropdown);
        }
    }
}
