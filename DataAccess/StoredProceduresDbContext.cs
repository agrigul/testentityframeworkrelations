﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Domain.Entities.StoredProcedures;

namespace DataAccess
{
    public class StoredProceduresDbContext : DbContext
    {
        public DbSet<StoredProcedureEntity> StoredProcedureEntitySet { get; set; }

        public StoredProceduresDbContext()
            : base("EntityRelationsTestsDB")
        {
            Database.SetInitializer<StoredProceduresDbContext>(null); // turn off DB creation

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<StoredProcedureEntity>().HasKey(x => x.Id).Property(x => x.Id).HasColumnName("Id");

            modelBuilder.Entity<StoredProcedureEntity>().Property(x => x.Age).HasColumnName("Age");
            modelBuilder.Entity<StoredProcedureEntity>().Property(x => x.Name).HasColumnName("Name");
                
                
            modelBuilder.Entity<StoredProcedureEntity>()
                .MapToStoredProcedures(map => map.Insert(i => i.HasName("spInsertSPEntity")
                                                            .Parameter(p => p.Name, "Name")
                                                            .Parameter(p => p.Age, "Age")
                                                            .Result(p => p.Id, "generated_id"))
                                                .Update(u => u.HasName("spUpdateSPEntity")
                                                            .Parameter(p => p.Id, "Id")
                                                            .Parameter(p => p.Name, "Name")
                                                            .Parameter(p => p.Age, "Age")
                                                            )
                                                .Delete(d => d.HasName("spDeleteSPEntity")
                                                            .Parameter(p => p.Id, "Id")));

            //modelBuilder.Entity<StoredProcedureEntity>().Property(c => c.Name).HasColumnName("Name");


        }


    }
}

