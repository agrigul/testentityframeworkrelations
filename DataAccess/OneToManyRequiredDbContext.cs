﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Domain.Entities.OneToMany;

namespace DataAccess
{
    public class OneToManyRequiredDbContext : DbContext
    {
        #region OneToOne

        public DbSet<MasterWithRequiredSlaveList> MasterWithRequiredSlaveListSet { get; set; }
        public DbSet<SlaveWithRequiredMaster> SlaveWithRequiredMasterSet { get; set; }

        #endregion


        public OneToManyRequiredDbContext()
            : base("EntityRelationsTestsDB")
        {
            Database.SetInitializer<OneToManyRequiredDbContext>(null); // turn off DB creation

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            OneToMany(modelBuilder);
        }
        

        private static void OneToMany(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<MasterWithRequiredSlaveList>().ToTable("MastersWithRequiredSlaveList");
            modelBuilder.Entity<MasterWithRequiredSlaveList>().HasKey(c => c.Id);
            modelBuilder.Entity<MasterWithRequiredSlaveList>().Property(c => c.Name).HasColumnName("Name");

            modelBuilder.Entity<SlaveWithRequiredMaster>().ToTable("SlavesWithRequiredMasterList");
            modelBuilder.Entity<SlaveWithRequiredMaster>().HasKey(c => c.Id);
            modelBuilder.Entity<SlaveWithRequiredMaster>().Property(c => c.Name).HasColumnName("Name");

            // 1-n, n-1  Relations.
            //----------------------------------------------------------------------------------------------------
            // --- OK. 
            //----------------------------------------------------------------------------------------------------

            BiDirectionalSuccessful2Select(modelBuilder);

            // Back Direction
            // BiDirectionalSuccessfulInnerJoin(modelBuilder);

            //----------------------------------------------------------------------------------------------------
            // --- Errors.
            //----------------------------------------------------------------------------------------------------
            
            // BiDirectionalFailedWrongPropertyMapping(modelBuilder);

            // BiDirectionalFailed3RdTableIsRequired(modelBuilder);
        }

        private static void BiDirectionalFailed3RdTableIsRequired(DbModelBuilder modelBuilder)
        {
        /* // No 3d table SlaveWithRequiredMasterList_MasterWithRequiredSlaveList for join.  Many-to-many relations.
             SQL: SELECT  MS.Id,  MS.Name
                    FROM  SlaveWithRequiredMasterListMasterWithRequiredSlaveList AS MSSM
                    INNER JOIN MastersWithRequiredSlaveList AS MS ON MSSM.MasterId = MS.Id
                    WHERE MSSM.SlaveId = 1             
             */
            // modelBuilder.Entity<SlaveWithRequiredMasterList>().HasMany(x => x.MasterList).WithMany(x => x.SlavesList).Map(x => x.MapLeftKey("SlaveId").MapRightKey("MasterId"));
        }

        private static void BiDirectionalFailedWrongPropertyMapping(DbModelBuilder modelBuilder)
        {
            /* --- Error:  Invalid column name 'SlaveWithRequiredMasterList_Id'
              SQL: SELECT  Id, Name, SlaveWithRequiredMasterList_Id FROM MastersWithRequiredSlaveList                           
             */
            modelBuilder.Entity<MasterWithRequiredSlaveList>().HasMany(x => x.SlavesList).WithOptional(x => x.Master).Map(x => x.MapKey("SlaveId")); // Same Quer
        }

        private static void BiDirectionalSuccessfulInnerJoin(DbModelBuilder modelBuilder)
        {
            /*
              SQL:  SELECT Id, Name, MasterId  FROM  SlavesWithRequiredMasterList
             +RPC: 	SELECT MS.Id, MS.Name
                    FROM  SlavesWithRequiredMasterList AS SM
                    INNER JOIN MastersWithRequiredSlaveList AS MS ON SM.MasterId = MS.Id
                    WHERE SM.Id = 1
            */
            modelBuilder.Entity<Domain.Entities.OneToMany.SlaveWithRequiredMaster>().HasRequired(x => x.Master).WithMany(x => x.SlavesList).Map(x => x.MapKey("MasterId")); // Same Quer
            // modelBuilder.Entity<MasterWithRequiredSlaveList>().HasMany(x => x.SlavesList).WithRequired(x => x.Master).Map(x => x.MapKey("MasterId")); // Same Query if get via Slaves
        }

        private static void BiDirectionalSuccessful2Select(DbModelBuilder modelBuilder)
        {
            /*
             SQL:  SELECT Id, Name  FROM  MastersWithRequiredSlaveList
            +RPC:  SELECT Id, Name, MasterId  FROM  SlavesWithRequiredMasterList WHERE MasterId = 1             
             */
            // modelBuilder.Entity<MasterWithRequiredSlaveList>().HasMany(x => x.SlavesList); // Same Quer. MasterId property is required
            // modelBuilder.Entity<MasterWithRequiredSlaveList>().HasMany(x => x.SlavesList).WithRequired(x => x.Master).HasForeignKey(x => x.MasterId); // Same Quer. MasterId property is required
             modelBuilder.Entity<MasterWithRequiredSlaveList>().HasMany(x => x.SlavesList).WithRequired(x => x.Master).Map(x => x.MapKey("MasterId")); // Same Quer
            // modelBuilder.Entity<MasterWithRequiredSlaveList>().HasMany(x => x.SlavesList).WithOptional(x => x.Master).Map(x => x.MapKey("MasterId")); // Same Quer
        }
    }
}

