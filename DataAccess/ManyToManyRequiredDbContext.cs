﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Domain.Entities.ManyToMany;


namespace DataAccess
{
    public class ManyToManyRequiredDbContext : DbContext
    {

        public DbSet<MasterWithRequiredSlaveList> MastersWithRequiredSlaveListSet { get; set; }
        public DbSet<SlaveWithRequiredMasterList> SlavesWithRequiredMasterListSet { get; set; }
        

        public ManyToManyRequiredDbContext()
            : base("EntityRelationsTestsDB")
        {
            Database.SetInitializer<ManyToManyRequiredDbContext>(null); // turn off DB creation
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            OneToMany(modelBuilder);
        }


        private static void OneToMany(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<MasterWithRequiredSlaveList>().ToTable("SimpleMastersManyToMany");
            modelBuilder.Entity<MasterWithRequiredSlaveList>().HasKey(c => c.Id);
            modelBuilder.Entity<MasterWithRequiredSlaveList>().Property(c => c.Name).HasColumnName("Name");

            modelBuilder.Entity<SlaveWithRequiredMasterList>().ToTable("SimpleSlavesManyToMany");
            modelBuilder.Entity<SlaveWithRequiredMasterList>().HasKey(c => c.Id);
            modelBuilder.Entity<SlaveWithRequiredMasterList>().Property(c => c.Name).HasColumnName("Name");

            // n-n  Relations.
            //----------------------------------------------------------------------------------------------------
            // --- OK. 
            //----------------------------------------------------------------------------------------------------

            /*
               SQL:  SELECT Id, Name  FROM  SimpleMastersManyToMany
              +RPC:  	SELECT SM.Id, SM.Name
                        FROM  MasterListSlaveList AS MSSM
                        INNER JOIN SimpleSlavesManyToMany AS SM ON MSSM.SlaveId = SM.Id
                        WHERE MSSM.MasterId = 1
               */

            modelBuilder.Entity<MasterWithRequiredSlaveList>()
                .HasMany(x => x.SlavesList)
                .WithMany(x => x.MasterList)
                .Map(x =>
                {
                    x.MapLeftKey("MasterId"); // because it's a left  column in table
                    x.MapRightKey("SlaveId"); // because it's a right column in table
                    x.ToTable("MasterListSlaveList");
                });
        }

    }
}

