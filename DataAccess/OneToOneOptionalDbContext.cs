﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Domain.Entities.OneToZeroOrOne;

namespace DataAccess
{
    public class OneToOneOptionalDbContext : DbContext
    {
        #region OneToOne

        public DbSet<MasterWithOptionalSlave> MasterWithOptionalSlaveSet { get; set; }
        public DbSet<SlaveWithOptionalMaster> SlaveWithOptionalMasterSet { get; set; }
        
        #endregion


        public OneToOneOptionalDbContext()
            : base("EntityRelationsTestsDB")
        {
            Database.SetInitializer<OneToOneOptionalDbContext>(null); // turn off DB creation

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            OneToOneBiDirection(modelBuilder);
        }



        private static void OneToOneBiDirection(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<MasterWithOptionalSlave>().ToTable("MastersWithOptionalSlave");
            modelBuilder.Entity<MasterWithOptionalSlave>().HasKey(c => c.Id);
            modelBuilder.Entity<MasterWithOptionalSlave>().Property(c => c.Name).HasColumnName("Name");

            modelBuilder.Entity<SlaveWithOptionalMaster>().ToTable("SlavesWithOptionalMaster");
            modelBuilder.Entity<SlaveWithOptionalMaster>().HasKey(c => c.Id);
            modelBuilder.Entity<SlaveWithOptionalMaster>().Property(c => c.Name).HasColumnName("Name");

            // 1-1 OneDirection Relations.
            //----------------------------------------------------------------------------------------------------
            // --- OK. 
            //----------------------------------------------------------------------------------------------------

            BiDirectionalSuccessul2Select(modelBuilder);

            BiDirectionalSuccessfulWithInnerJoinsAndSubSelect(modelBuilder);

            BiDirectionalSuccessfulWithOuterJoin(modelBuilder);


            // also works: modelBuilder.Entity<MasterWithOptionalSlave>().HasOptional(x => x.Slave).WithMany().Map(x => x.MapKey("MasterId")); 

        }

        private static void BiDirectionalSuccessfulWithOuterJoin(DbModelBuilder modelBuilder)
        {
            /*
            SQL:  SELECT  MS.Id, MS.Name, SM.Id as SmId
                FROM  MastersWithOptionalSlave AS MS
                LEFT OUTER JOIN SlavesWithOptionalMaster AS SM
                        ON (SM.MasterId IS NOT NULL) 
                AND (MS.Id = SM.MasterId)
            * 
            +RPC: SELECT SM.Id, SM.Name, SM.MasterId    
                FROM SlavesWithOptionalMaster AS SM
                WHERE (SM.MasterId IS NOT NULL) 
                AND (SM.MasterId = 2)
            * 
            */
            modelBuilder.Entity<MasterWithOptionalSlave>().HasOptional(x => x.Slave).WithOptionalPrincipal(x => x.Master).Map(x => x.MapKey("MasterId")); // Same query
            // modelBuilder.Entity<SlaveWithOptionalMaster>().HasOptional(x => x.Master).WithOptionalDependent(x => x.Slave).Map(x=>x.MapKey("MasterId")); // Same query
        }

        private static void BiDirectionalSuccessfulWithInnerJoinsAndSubSelect(DbModelBuilder modelBuilder)
        {
            /*
            SQL: SELECT  Id, Name, SlaveId FROM MastersWithOptionalSlave
            +RMC:  SELECT 
                    Join1.SmId AS Id, 
                    Join1.SmName AS Name, 
                    Join1.MS2Id AS MS2Id
                FROM  MastersWithOptionalSlave AS MS1
                INNER JOIN  (
                                SELECT SM.Id as SmId, SM.Name as SmName,  MS2.Id as MS2Id							
                                FROM  SlavesWithOptionalMaster AS SM
                                LEFT OUTER JOIN MastersWithOptionalSlave 
                                AS MS2 
                                ON  (MS2.SlaveId  IS NOT NULL) And (SM.Id = MS2.SlaveId)
                            )  AS Join1  ON MS1.SlaveId = Join1.SmId

                WHERE (MS1.SlaveId IS NOT NULL) AND (MS1.Id = 2)

            */
            modelBuilder.Entity<MasterWithOptionalSlave>().HasOptional(x => x.Slave).WithOptionalDependent(x => x.Master).Map(x => x.MapKey("SlaveId")); // Same query
            // modelBuilder.Entity<SlaveWithOptionalMaster>().HasOptional(x => x.Master).WithOptionalPrincipal(x => x.Slave).Map(x => x.MapKey("SlaveId")); // Same query
        }

        private static void BiDirectionalSuccessul2Select(DbModelBuilder modelBuilder)
        {
            /*                          
                + SQL: SELECT  Id, Name  FROM MastersWithOptionalSlave
                + RPC: SELECT  Id, Name  FROM SlavesWithOptionalMaster             
            */
            modelBuilder.Entity<MasterWithOptionalSlave>().HasOptional(x => x.Slave).WithRequired(x => x.Master); // Same query
            // modelBuilder.Entity<SlaveWithOptionalMaster>().HasOptional(x => x.Master).WithRequired(x => x.Slave); // Same query
        }
    }
}

