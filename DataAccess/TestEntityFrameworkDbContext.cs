﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Domain.Entities.OneToZeroOrOne;
using Domain.Entities.TestingEntityFramework;

namespace DataAccess
{
    public class TestEntityFrameworkDbContext : DbContext
    {
        #region OneToOne

        public DbSet<Book> BooksSet { get; set; }
        public DbSet<Student> StudentsSet { get; set; }
        
        #endregion


        public TestEntityFrameworkDbContext()
            : base("EntityRelationsTestsDB")
        {
            Database.SetInitializer<TestEntityFrameworkDbContext>(null); // turn off DB creation

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            OneToOneBiDirection(modelBuilder);
        }



        private static void OneToOneBiDirection(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Book>().ToTable("1Books");
            modelBuilder.Entity<Book>().HasKey(c => c.BookId);
            modelBuilder.Entity<Book>().Property(c => c.BookId).HasColumnName("Id");
            modelBuilder.Entity<Book>().Property(c => c.BookName).HasColumnName("Name");

            modelBuilder.Entity<Student>().ToTable("1Students");
            modelBuilder.Entity<Student>().HasKey(c => c.StudentId);
            modelBuilder.Entity<Student>().Property(c => c.StudentId).HasColumnName("Id");
            modelBuilder.Entity<Student>().Property(c => c.StudentName).HasColumnName("Name");


           // modelBuilder.Entity<Student>().HasOptional(x => x.Book).WithOptionalPrincipal(x => x.Student).Map(x => x.MapKey("StudentId"));
            modelBuilder.Entity<Student>().HasOptional(x => x.Book).WithOptionalPrincipal().Map(x => x.MapKey("StudentId"));
           //modelBuilder.Entity<Student>().HasOptional(x => x.Book).WithMany().Map(x => x.MapKey("BookId")); 

        }

        private static void BiDirectionalSuccessfulWithOuterJoin(DbModelBuilder modelBuilder)
        {
            /*
            SQL:  SELECT  MS.Id, MS.Name, SM.Id as SmId
                FROM  MastersWithOptionalSlave AS MS
                LEFT OUTER JOIN SlavesWithOptionalMaster AS SM
                        ON (SM.MasterId IS NOT NULL) 
                AND (MS.Id = SM.MasterId)
            * 
            +RPC: SELECT SM.Id, SM.Name, SM.MasterId    
                FROM SlavesWithOptionalMaster AS SM
                WHERE (SM.MasterId IS NOT NULL) 
                AND (SM.MasterId = 2)
            * 
            */
            modelBuilder.Entity<MasterWithOptionalSlave>().HasOptional(x => x.Slave).WithOptionalPrincipal(x => x.Master).Map(x => x.MapKey("MasterId")); // Same query
            // modelBuilder.Entity<SlaveWithOptionalMaster>().HasOptional(x => x.Master).WithOptionalDependent(x => x.Slave).Map(x=>x.MapKey("MasterId")); // Same query
        }

        private static void BiDirectionalSuccessfulWithInnerJoinsAndSubSelect(DbModelBuilder modelBuilder)
        {
            /*
            SQL: SELECT  Id, Name, SlaveId FROM MastersWithOptionalSlave
            +RMC:  SELECT 
                    Join1.SmId AS Id, 
                    Join1.SmName AS Name, 
                    Join1.MS2Id AS MS2Id
                FROM  MastersWithOptionalSlave AS MS1
                INNER JOIN  (
                                SELECT SM.Id as SmId, SM.Name as SmName,  MS2.Id as MS2Id							
                                FROM  SlavesWithOptionalMaster AS SM
                                LEFT OUTER JOIN MastersWithOptionalSlave 
                                AS MS2 
                                ON  (MS2.SlaveId  IS NOT NULL) And (SM.Id = MS2.SlaveId)
                            )  AS Join1  ON MS1.SlaveId = Join1.SmId

                WHERE (MS1.SlaveId IS NOT NULL) AND (MS1.Id = 2)

            */
            modelBuilder.Entity<MasterWithOptionalSlave>().HasOptional(x => x.Slave).WithOptionalDependent(x => x.Master).Map(x => x.MapKey("SlaveId")); // Same query
            // modelBuilder.Entity<SlaveWithOptionalMaster>().HasOptional(x => x.Master).WithOptionalPrincipal(x => x.Slave).Map(x => x.MapKey("SlaveId")); // Same query
        }

        private static void BiDirectionalSuccessul2Select(DbModelBuilder modelBuilder)
        {
            /*                          
                + SQL: SELECT  Id, Name  FROM MastersWithOptionalSlave
                + RPC: SELECT  Id, Name  FROM SlavesWithOptionalMaster             
            */
            modelBuilder.Entity<MasterWithOptionalSlave>().HasOptional(x => x.Slave).WithRequired(x => x.Master); // Same query
            // modelBuilder.Entity<SlaveWithOptionalMaster>().HasOptional(x => x.Master).WithRequired(x => x.Slave); // Same query
        }
    }
}

