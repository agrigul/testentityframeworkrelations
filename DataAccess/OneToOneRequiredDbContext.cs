﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Domain.Entities.OneToOne.BiDirectional;
using Domain.Entities.OneToOne.OneDirectional;
using Domain.Entities.OneToZeroOrOne;

namespace DataAccess
{
    public class OneToOneRequiredDbContext : DbContext
    {
        #region OneToOne

        public DbSet<MasterWithRequiredSlave> MasterWithRequiredSlaveSet { get; set; }
        public DbSet<SlaveWithRequiredMaster> SlaveWithRequiredMasterSet { get; set; }


        public DbSet<MasterWithRequiredSimpleSlave> MasterWithRequiredSimpleSlaveSet { get; set; }
        public DbSet<SimpleSlave> SimpleSlaveSet { get; set; }

        #endregion


        public OneToOneRequiredDbContext()
            : base("EntityRelationsTestsDB")
        {
            Database.SetInitializer<OneToOneRequiredDbContext>(null); // turn off DB creation

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            OneToOneSingleDirection(modelBuilder);
            OneToOneBiDirection(modelBuilder);
        }



        private static void OneToOneSingleDirection(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MasterWithRequiredSimpleSlave>().ToTable("MasterWithRequiredSimpleSlave");
            modelBuilder.Entity<MasterWithRequiredSimpleSlave>().HasKey(c => c.Id);
            modelBuilder.Entity<MasterWithRequiredSimpleSlave>().Property(c => c.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<MasterWithRequiredSimpleSlave>().Property(c => c.Name).HasColumnName("Name");

            modelBuilder.Entity<SimpleSlave>().ToTable("SimpleSlaves");
            modelBuilder.Entity<SimpleSlave>().HasKey(c => c.Id);
            modelBuilder.Entity<SimpleSlave>().Property(c => c.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<SimpleSlave>().Property(c => c.Name).HasColumnName("Name");

            // 1-1 OneDirection Relations.
            //----------------------------------------------------------------------------------------------------
            // --- OK. NO SimpleSlave.Master propery
            //----------------------------------------------------------------------------------------------------

            OneDirectionSuccessfulWith2Selects(modelBuilder);

            // OneDirectionSuccessfulWithInnerJoiunsAndSubSelects(modelBuilder);


            //----------------------------------------------------------------------------------------------------------------------------------
            //   Error: Invalid column name 'Slave_Id'.  HAS MasterWithRequiredSimpleSlave.Slave but no mapping
            //----------------------------------------------------------------------------------------------------------------------------------

            // OneDirectionFailedWith1Select(modelBuilder);

            // OneDirectionFailedWithJoin(modelBuilder);
        }
        #region OneDirectional mapping
        private static void OneDirectionFailedWithJoin(DbModelBuilder modelBuilder)
        {
            // Error: Invalid column name 'Slave_Id'.  HAS MasterWithRequiredSimpleSlave.Slave but no mapping
            /*  
                SQL: SELECT  ms.Id, ms.Name, ss.Id  
                    FROM  MasterWithRequiredSimpleSlave as ms 
                    LEFT OUTER JOIN SimpleSlaves AS ss ON ms.Id = ss.SlaveId
            */
            modelBuilder.Entity<MasterWithRequiredSimpleSlave>()
                .HasRequired(x => x.Slave)
                .WithRequiredPrincipal()
                .Map(x => x.MapKey("SlaveId")); // Same Query
        }

        private static void OneDirectionFailedWith1Select(DbModelBuilder modelBuilder)
        {
            // Error: Invalid column name 'Slave_Id'.  HAS MasterWithRequiredSimpleSlave.Slave but no mapping
            /*  
                SQL: SELECT Id, Name, Slave_Id FROM MasterWithRequiredSimpleSlave
            */
            modelBuilder.Entity<MasterWithRequiredSimpleSlave>().HasRequired(x => x.Slave); // Same Query
        }

        private static void OneDirectionSuccessfulWithInnerJoiunsAndSubSelects(DbModelBuilder modelBuilder)
        {
            /*
            SQL: SELECT Id, Name, SlaveId FROM MasterWithRequiredSimpleSlave 
            +RPC: SELECT 
                    Join1.SmId AS Id, 
                    Join1.SmName AS Name, 
                    Join1.MS2Id AS MS2Id
                FROM  MasterWithRequiredSimpleSlave AS MS1
                INNER JOIN  (
                                SELECT SM.Id as SmId, SM.Name as SmName,  MS2.Id as MS2Id							
                                FROM  SimpleSlaves AS SM
                                LEFT OUTER JOIN MasterWithRequiredSimpleSlave AS MS2 
                                ON SM.Id = MS2.SlaveId 
                            )  AS Join1  ON MS1.SlaveId = Join1.SmId

                WHERE MS1.Id = 3
            */
            modelBuilder.Entity<MasterWithRequiredSimpleSlave>()
                .HasRequired(x => x.Slave)
                .WithOptional()
                .Map(x => x.MapKey("SlaveId")); // Same Query
        }

        private static void OneDirectionSuccessfulWith2Selects(DbModelBuilder modelBuilder)
        {
            /* 
            *  Need the Ids of Master and Slave to be equal. Query is equal for all.
            *  
                SQL:  SELECT Id, Name FROM MasterWithRequiredSimpleSlave. 
                +RPC: SELECT Id, Name  FROM  SimpleSlaves as sm  WHERE Id = 1
            */
             modelBuilder.Entity<MasterWithRequiredSimpleSlave>().HasRequired(x => x.Slave).WithOptional(); // Same Query
            // modelBuilder.Entity<MasterWithRequiredSimpleSlave>().HasRequired(x => x.Slave).WithRequiredDependent(); // Same Query
            // modelBuilder.Entity<MasterWithRequiredSimpleSlave>().HasRequired(x => x.Slave).WithRequiredPrincipal();  // Same Query
        }
        
        #endregion OneDirectional mapping

        private static void OneToOneBiDirection(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MasterWithRequiredSlave>().ToTable("MastersWithRequiredSlave");
            modelBuilder.Entity<MasterWithRequiredSlave>().HasKey(c => c.Id);
            modelBuilder.Entity<MasterWithRequiredSlave>().Property(c => c.Name).HasColumnName("Name");

            modelBuilder.Entity<SlaveWithRequiredMaster>().ToTable("SlavesWithRequiredMaster");
            modelBuilder.Entity<SlaveWithRequiredMaster>().HasKey(c => c.Id);
            modelBuilder.Entity<SlaveWithRequiredMaster>().Property(c => c.Name).HasColumnName("Name");


            // 1-1 BiDirectonal Relations.

            // -------------------------------------------------------------------------------------------------------------------
            //  OK. 
            // -------------------------------------------------------------------------------------------------------------------

            BiDirectionalMappingSuccessfulWith2Selects(modelBuilder);

            //BiRiectionalMappingSuccessfulWithOuterJoin(modelBuilder);

            //BiDirectionalMappingSuccessfulWithInnerJoinsAndSubSelect(modelBuilder);


            // ----------------------------------------------------------------------------------------------------------------------------
            //   Error requests:
            //-----------------------------------------------------------------------------------------------------------------------------


            //BiDirectionalMappingFailedWith2Selects(modelBuilder);

            //BiDirectionalMappingFailedWithInnerJoinsAndSubSelect(modelBuilder);

            //BiDirectionalMappingFailedWithOuterJoin(modelBuilder);
        }


        #region BiDirectional mapping

        private static void BiDirectionalMappingFailedWithOuterJoin(DbModelBuilder modelBuilder)
        {
            /*
            SELECT MS.Id, MS.Name, SM.Id    
            FROM  MastersWithRequiredSlave AS MS
            LEFT OUTER JOIN  SlavesWithRequiredMaster AS SM ON MS.Id = SM.SlaveId 
            */
            modelBuilder.Entity<SlaveWithRequiredMaster>()
                .HasRequired(x => x.Master)
                .WithOptional(x => x.Slave)
                .Map(x => x.MapKey("SlaveId"));
        }

        private static void BiDirectionalMappingFailedWithInnerJoinsAndSubSelect(DbModelBuilder modelBuilder)
        {
            /*
                SQL:  SELECT Id, Name, SlaveId FROM MastersWithRequiredSlave
                +RPC: SELECT 
                        Join1.SmId AS Id, 
                        Join1.SmName AS Name, 
                        Join1.Master_Id AS Master_Id, 
                        Join1.MS2Id AS MS2Id
                        FROM  MastersWithRequiredSlave AS MS1
                        INNER JOIN  (
                                    SELECT SM.Id as SmId, SM.Name as SmName, SM.Master_Id, MS2.Id as MS2Id							
                                    FROM  SlavesWithRequiredMaster AS SM
                                    LEFT OUTER JOIN MastersWithRequiredSlave AS MS2 
                                    ON SM.Id = MS2.SlaveId 
                                )  AS Join1  ON MS1.SlaveId = Join1.SmId

                        WHERE MS1.Id = 3
                */
            modelBuilder.Entity<MasterWithRequiredSlave>()
                .HasRequired(x => x.Slave)
                .WithOptional()
                .Map(x => x.MapKey("SlaveId")); // Same Query
        }

        private static void BiDirectionalMappingFailedWith2Selects(DbModelBuilder modelBuilder)
        {
            // --- Error: Invalid column name 'Master_Id'.  HAS SlaveWithRequiredMaster.Master propery
            /*
                SQL: SELECT Id, Name FROM MastersWithRequiredSlave
                +RPC: SELECT  Id, Name, Master_Id SlavesWithRequiredMaster as sm  WHERE Id = 3
            */
            modelBuilder.Entity<MasterWithRequiredSlave>().HasRequired(x => x.Slave).WithOptional();
            // modelBuilder.Entity<MasterWithRequiredSlave>().HasRequired(x => x.Slave).WithRequiredDependent(); // Same query
            // modelBuilder.Entity<MasterWithRequiredSlave>().HasRequired(x => x.Slave).WithRequiredPrincipal(); // Same Query



            // --- Error: Invalid column name 'Slave_Id'. HAS MasterWithRequiredSlave.Slave propery
            /* 
                SQL:  SELECT  Id, Name, Slave_Id FROM MastersWithRequiredSlave 
             */
            // modelBuilder.Entity<SlaveWithRequiredMaster>().HasRequired(x => x.Master).WithOptional();
            // modelBuilder.Entity<SlaveWithRequiredMaster>().HasRequired(x => x.Master).WithRequiredDependent(); // Same Query
            // modelBuilder.Entity<SlaveWithRequiredMaster>().HasRequired(x => x.Master).WithRequiredPrincipal(); // Same Query
        }

        private static void BiDirectionalMappingSuccessfulWithInnerJoinsAndSubSelect(DbModelBuilder modelBuilder)
        {
            /*
                * Cant'b used  with equal id only of master and slave. Need to be set SlaveId and MasterId in tables. Like many-to-many. The reason is MapKey("SlaveId")
                * 
            SQL: SELECT Id, Name, SlaveId FROM MastersWithRequiredSlave
            RPC: SELECT 
                    Join1.SmId AS Id, 
                    Join1.SmName AS Name, 
                    Join1.MS2Id AS MS2Id
                FROM  MastersWithRequiredSlave AS MS1
                INNER JOIN  (
                                SELECT SM.Id as SmId, SM.Name as SmName, MS2.Id as MS2Id							
                                FROM  SlavesWithRequiredMaster AS SM
                                LEFT OUTER JOIN MastersWithRequiredSlave AS MS2 
                                ON SM.Id = MS2.SlaveId 
                            )  AS Join1  ON MS1.SlaveId = Join1.SmId
                WHERE MS1.Id = 3
            */
            modelBuilder.Entity<MasterWithRequiredSlave>()
                .HasRequired(x => x.Slave)
                .WithOptional(x => x.Master)
                .Map(x => x.MapKey("SlaveId")); // Same Query
            // modelBuilder.Entity<MasterWithRequiredSlave>().HasRequired(x => x.Slave).WithRequiredDependent(x => x.Master).Map(x => x.MapKey("SlaveId"));  // Same Query
        }

        private static void BiRiectionalMappingSuccessfulWithOuterJoin(DbModelBuilder modelBuilder)
        {
            /*  
                * Cant'b used only with equal id of master and slave. Need to be set SlaveId and MasterId in tables. Like many-to-many
                * 
                SQL: SELECT  MS.Id, MS.Name, SM.Id
                    FROM  MastersWithRequiredSlave AS MS
                    LEFT OUTER JOIN SlavesWithRequiredMaster AS SM ON MS.Id = SM.MasterId
             
                + RPC: SELECT Id, Name, MasterId  FROM  SlavesWithRequiredMaster as sm  WHERE Id = 3
                */
            modelBuilder.Entity<MasterWithRequiredSlave>()
                .HasRequired(x => x.Slave)
                .WithRequiredPrincipal(x => x.Master)
                .Map(x => x.MapKey("MasterId")); // Same Query
            // modelBuilder.Entity<SlaveWithRequiredMaster>().HasRequired(x => x.Master).WithOptional(x => x.Slave).Map(x => x.MapKey("MasterId")); // Same Query
            // modelBuilder.Entity<SlaveWithRequiredMaster>().HasRequired(x => x.Master).WithRequiredDependent(x => x.Slave).Map(x => x.MapKey("MasterId")); // Same Query
        }

        private static void BiDirectionalMappingSuccessfulWith2Selects(DbModelBuilder modelBuilder)
        {
            /* 
                 SQL: SELECT Id, Name FROM MastersWithRequiredSlave 
               + RPC: SELECT  Id, Name FROM SlavesWithRequiredMaster WHERE Id = 3
             */
            modelBuilder.Entity<MasterWithRequiredSlave>().HasRequired(x => x.Slave); // Same Query
            // modelBuilder.Entity<MasterWithRequiredSlave>().HasRequired(x => x.Slave).WithOptional(x => x.Master); // Same Query
            // modelBuilder.Entity<MasterWithRequiredSlave>().HasRequired(x => x.Slave).WithRequiredPrincipal(x => x.Master); // Same Query
            // modelBuilder.Entity<SlaveWithRequiredMaster>().HasRequired(x => x.Master); // Same Query
            // modelBuilder.Entity<SlaveWithRequiredMaster>().HasRequired(x => x.Master).WithRequiredDependent(x => x.Slave);  // Same Query
            // modelBuilder.Entity<SlaveWithRequiredMaster>().HasRequired(x => x.Master).WithRequiredPrincipal(x => x.Slave);  // Same Query
        }

        #endregion BiDirectional mapping

        private static void MasterWithOptionalSlave(DbModelBuilder modelBuilder)
        {
            // one-to-one  or one-to-zero   relations
            modelBuilder.Entity<MasterWithOptionalSlave>().ToTable("MastersWithOptionalSlave");
            modelBuilder.Entity<MasterWithOptionalSlave>().HasKey(c => c.Id);
            modelBuilder.Entity<MasterWithOptionalSlave>().Property(c => c.Name).HasColumnName("Name");


            modelBuilder.Entity<SlaveWithOptionalMaster>().ToTable("SlavesWithOptionalMaster");
            modelBuilder.Entity<SlaveWithOptionalMaster>().HasKey(c => c.Id);
            modelBuilder.Entity<SlaveWithOptionalMaster>().Property(c => c.Name).HasColumnName("Name");


            // ERROR on read: The principal end of this association must be explicitly configured using either the relationship fluent API or data annotations.
            // modelBuilder.Entity<MasterWithOptionalSlave>().HasOptional(c => c.Slave);
            // modelBuilder.Entity<SlaveWithOptionalMaster>().HasOptional(c => c.Master);

            // ERROR on read:  Unable to determine the principal end of an association between the types 'Domain.Entities.OneToOne.Slave' and 'Domain.Entities.OneToOne.MasterWithRequiredSlave'
            // The principal end of this association must be explicitly configured using either the relationship fluent API or data annotations.
            // modelBuilder.Entity<MasterWithOptionalSlave>().HasOptional(c => c.Slave).WithOptionalDependent();
            // modelBuilder.Entity<SlaveWithOptionalMaster>().HasOptional(c => c.Master).WithOptionalDependent();

            modelBuilder.Entity<MasterWithOptionalSlave>().HasOptional(c => c.Slave).WithRequired();  // 1 --> 0..1
            //modelBuilder.Entity<SlaveWithOptionalMaster>().HasRequired(c => c.Master).WithOptional(); // 0..1 --> 1




        }

    }
}

